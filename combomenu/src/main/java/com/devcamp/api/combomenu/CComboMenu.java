package com.devcamp.api.combomenu;

import java.text.NumberFormat;
import java.util.Locale;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class CComboMenu {
    String kichCo;
    String duongKinh;
    int suongNuong;
    String salad;
    int nuocNgot;
    long donGia;

    Locale lc = new Locale("nv","VN");
    NumberFormat nf = NumberFormat.getCurrencyInstance(lc);
    

    public CComboMenu() {
    }

    public CComboMenu(String kichCo, String duongKinh, int suongNuong) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suongNuong = suongNuong;
        this.salad = "500g";
        this.nuocNgot = 4;
        this.donGia = 250000;
    }

    public CComboMenu(String kichCo, String duongKinh) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suongNuong = 4;
        this.salad = "300g";
        this.nuocNgot = 3;
        this.donGia = 200000;
    }

    public CComboMenu(String kichCo) {
        this.kichCo = kichCo;
        this.duongKinh = "20cm";
        this.suongNuong = 2;
        this.salad = "200g";
        this.nuocNgot = 2;
        this.donGia = 150000;
    }

    public String getKichCo() {
        return kichCo;
    }

    public void setKichCo(String kichCo) {
        this.kichCo = kichCo;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuongNuong() {
        return suongNuong;
    }

    public void setSuongNuong(int suongNuong) {
        this.suongNuong = suongNuong;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public int getNuocNgot() {
        return nuocNgot;
    }

    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public long getDonGia() {
        return donGia;
    }

    public void setDonGia(long donGia) {
        this.donGia = donGia;
    }

    
  
    
}