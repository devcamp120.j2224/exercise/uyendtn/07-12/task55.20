package com.devcamp.api.combomenu;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/combomenu")
    public static ArrayList<CComboMenu> getComboMenu() {
        
        ArrayList<CComboMenu> comboMenu = new ArrayList<CComboMenu>();
        CComboMenu comboS = new CComboMenu("S");
        CComboMenu comboM = new CComboMenu("M", "25cm");
        CComboMenu comboL = new CComboMenu("L", "30cm", 8);
        comboMenu.add(comboS);
        comboMenu.add(comboM);
        comboMenu.add(comboL);

        return comboMenu;
    } 
}
